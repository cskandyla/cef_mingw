# Compiler flags...
CPP_COMPILER = g++
C_COMPILER = gcc

# Include paths...
Debug_Include_Path=-I"..\..\..\third_party\wtl\include" -I"." -I"..\..\..\..\..\Program Files (x86)\Windows Kits\8.0\Include\shared" -I"..\..\..\..\..\Program Files (x86)\Windows Kits\8.0\Include\um" -I"..\..\..\..\..\Program Files (x86)\Windows Kits\8.0\Include\winrt" -I"..\..\..\..\..\Program Files\Microsoft Visual Studio 10.0\\VC\atlmfc\include" 
Debug_Include_Path=-I"..\..\..\third_party\wtl\include" -I"." -I"..\..\..\..\..\Program Files (x86)\Windows Kits\8.0\Include\shared" -I"..\..\..\..\..\Program Files (x86)\Windows Kits\8.0\Include\um" -I"..\..\..\..\..\Program Files (x86)\Windows Kits\8.0\Include\winrt" -I"..\..\..\..\..\Program Files\Microsoft Visual Studio 10.0\\VC\atlmfc\include" 
Release_Include_Path=-I"..\..\..\third_party\wtl\include" -I"." -I"..\..\..\..\..\Program Files (x86)\Windows Kits\8.0\Include\shared" -I"..\..\..\..\..\Program Files (x86)\Windows Kits\8.0\Include\um" -I"..\..\..\..\..\Program Files (x86)\Windows Kits\8.0\Include\winrt" -I"..\..\..\..\..\Program Files\Microsoft Visual Studio 10.0\\VC\atlmfc\include" 
Release_Include_Path=-I"..\..\..\third_party\wtl\include" -I"." -I"..\..\..\..\..\Program Files (x86)\Windows Kits\8.0\Include\shared" -I"..\..\..\..\..\Program Files (x86)\Windows Kits\8.0\Include\um" -I"..\..\..\..\..\Program Files (x86)\Windows Kits\8.0\Include\winrt" -I"..\..\..\..\..\Program Files\Microsoft Visual Studio 10.0\\VC\atlmfc\include" 

# Library paths...
Debug_Library_Path=-L"..\..\..\..\..\Program Files (x86)\Windows Kits\8.0\Lib\win8\um\gccx86" -L"Debug"
Debug_Library_Path=-L"..\..\..\..\..\Program Files (x86)\Windows Kits\8.0\Lib\win8\um\gccx64" -L"Debug"
Release_Library_Path=-L"..\..\..\..\..\Program Files (x86)\Windows Kits\8.0\Lib\win8\um\gccx86" 
Release_Library_Path=-L"..\..\..\..\..\Program Files (x86)\Windows Kits\8.0\Lib\win8\um\gccx64" 

# Additional libraries...
Debug_Libraries= -lwininet -ldnsapi -lversion -lmsimg32 -lws2_32 -lusp10 -lpsapi  -lwinmm -lshlwapi -lkernel32 -lgdi32 -lwinspool -lcomdlg32 -ladvapi32 -lshell32 -lole32 -loleaut32 -luser32 -luuid -lodbc32 -lodbccp32  -lcomctl32 -lrpcrt4 -lopengl32 -lglu32  -llibcef -llibcef_dll_wrapper
Debug_Libraries= -lwininet -ldnsapi -lversion -lmsimg32 -lws2_32 -lusp10 -lpsapi -lwinmm -lshlwapi -lkernel32 -lgdi32 -lwinspool -lcomdlg32 -ladvapi32 -lshell32 -lole32 -loleaut32 -luser32 -luuid -lodbc32 -lodbccp32  -lcomctl32 -lrpcrt4 -lopengl32 -lglu32 -llibcef -llibcef_dll_wrapper
Release_Libraries=-Wl,--start-group -lwininet -ldnsapi -lversion -lmsimg32 -lws2_32 -lusp10 -lpsapi -ldbghelp -lwinmm -lshlwapi -lkernel32 -lgdi32 -lwinspool -lcomdlg32 -ladvapi32 -lshell32 -lole32 -loleaut32 -luser32 -luuid -lodbc32 -lodbccp32 -ldelayimp -lcomctl32 -lrpcrt4 -lopengl32 -lglu32  -l$(NOINHERIT)  -Wl,--end-group
Release_Libraries=-Wl,--start-group -lwininet -ldnsapi -lversion -lmsimg32 -lws2_32 -lusp10 -lpsapi -ldbghelp -lwinmm -lshlwapi -lkernel32 -lgdi32 -lwinspool -lcomdlg32 -ladvapi32 -lshell32 -lole32 -loleaut32 -luser32 -luuid -lodbc32 -lodbccp32 -ldelayimp -lcomctl32 -lrpcrt4 -lopengl32 -lglu32 -llibcef -l$(NOINHERIT)  -Wl,--end-group

# Preprocessor definitions...
Debug_Preprocessor_Definitions=-D _DEBUG -D ANGLE_DX11 -D _WIN32_WINNT=0x0602 -D WINVER=0x0602 -D GCC_BUILD -D _WINDOWS -D NOMINMAX -D PSAPI_VERSION=1 -D _CRT_RAND_S -D CERT_CHAIN_PARA_HAS_EXTRA_FIELDS -D WIN32_LEAN_AND_MEAN -D _ATL_NO_OPENGL -D _HAS_EXCEPTIONS=0 -D _SECURE_ATL -D CHROMIUM_BUILD -D TOOLKIT_VIEWS=1 -D USE_LIBJPEG_TURBO=1 -D ENABLE_ONE_CLICK_SIGNIN -D ENABLE_REMOTING=1 -D ENABLE_WEBRTC=1 -D ENABLE_PEPPER_CDMS -D ENABLE_CONFIGURATION_POLICY -D ENABLE_INPUT_SPEECH -D ENABLE_NOTIFICATIONS -D ENABLE_GPU=1 -D ENABLE_EGLIMAGE=1 -D __STD_C -D _CRT_SECURE_NO_DEPRECATE -D _SCL_SECURE_NO_DEPRECATE -D NTDDI_VERSION=0x06020000 -D ENABLE_TASK_MANAGER=1 -D ENABLE_EXTENSIONS=1 -D ENABLE_PLUGIN_INSTALLATION=1 -D ENABLE_PLUGINS=1 -D ENABLE_SESSION_SERVICE=1 -D ENABLE_THEMES=1 -D ENABLE_AUTOFILL_DIALOG=1 -D ENABLE_BACKGROUND=1 -D ENABLE_AUTOMATION=1 -D ENABLE_GOOGLE_NOW=1 -D CLD_VERSION=1 -D ENABLE_FULL_PRINTING=1 -D ENABLE_PRINTING=1 -D ENABLE_SPELLCHECK=1 -D ENABLE_CAPTIVE_PORTAL_DETECTION=1 -D ENABLE_APP_LIST=1 -D ENABLE_SETTINGS_APP=1 -D ENABLE_MANAGED_USERS=1 -D ENABLE_MDNS=1 -D USING_CEF_SHARED -D __STDC_CONSTANT_MACROS -D __STDC_FORMAT_MACROS -D DYNAMIC_ANNOTATIONS_ENABLED=1 -D WTF_USE_DYNAMIC_ANNOTATIONS=1 
Debug_Preprocessor_Definitions=-D _DEBUG -D ANGLE_DX11 -D _WIN32_WINNT=0x0602 -D WINVER=0x0602 -D GCC_BUILD -D _WINDOWS -D NOMINMAX -D PSAPI_VERSION=1 -D _CRT_RAND_S -D CERT_CHAIN_PARA_HAS_EXTRA_FIELDS -D WIN32_LEAN_AND_MEAN -D _ATL_NO_OPENGL -D _HAS_EXCEPTIONS=0 -D _SECURE_ATL -D CHROMIUM_BUILD -D TOOLKIT_VIEWS=1 -D USE_LIBJPEG_TURBO=1 -D ENABLE_ONE_CLICK_SIGNIN -D ENABLE_REMOTING=1 -D ENABLE_WEBRTC=1 -D ENABLE_PEPPER_CDMS -D ENABLE_CONFIGURATION_POLICY -D ENABLE_INPUT_SPEECH -D ENABLE_NOTIFICATIONS -D ENABLE_GPU=1 -D ENABLE_EGLIMAGE=1 -D __STD_C -D _CRT_SECURE_NO_DEPRECATE -D _SCL_SECURE_NO_DEPRECATE -D NTDDI_VERSION=0x06020000 -D ENABLE_TASK_MANAGER=1 -D ENABLE_EXTENSIONS=1 -D ENABLE_PLUGIN_INSTALLATION=1 -D ENABLE_PLUGINS=1 -D ENABLE_SESSION_SERVICE=1 -D ENABLE_THEMES=1 -D ENABLE_AUTOFILL_DIALOG=1 -D ENABLE_BACKGROUND=1 -D ENABLE_AUTOMATION=1 -D ENABLE_GOOGLE_NOW=1 -D CLD_VERSION=1 -D ENABLE_FULL_PRINTING=1 -D ENABLE_PRINTING=1 -D ENABLE_SPELLCHECK=1 -D ENABLE_CAPTIVE_PORTAL_DETECTION=1 -D ENABLE_APP_LIST=1 -D ENABLE_SETTINGS_APP=1 -D ENABLE_MANAGED_USERS=1 -D ENABLE_MDNS=1 -D USING_CEF_SHARED -D __STDC_CONSTANT_MACROS -D __STDC_FORMAT_MACROS -D DYNAMIC_ANNOTATIONS_ENABLED=1 -D WTF_USE_DYNAMIC_ANNOTATIONS=1 
Release_Preprocessor_Definitions=-D ANGLE_DX11 -D _WIN32_WINNT=0x0602 -D WINVER=0x0602 -D GCC_BUILD -D _WINDOWS -D NOMINMAX -D PSAPI_VERSION=1 -D _CRT_RAND_S -D CERT_CHAIN_PARA_HAS_EXTRA_FIELDS -D WIN32_LEAN_AND_MEAN -D _ATL_NO_OPENGL -D _HAS_EXCEPTIONS=0 -D _SECURE_ATL -D CHROMIUM_BUILD -D TOOLKIT_VIEWS=1 -D USE_LIBJPEG_TURBO=1 -D ENABLE_ONE_CLICK_SIGNIN -D ENABLE_REMOTING=1 -D ENABLE_WEBRTC=1 -D ENABLE_PEPPER_CDMS -D ENABLE_CONFIGURATION_POLICY -D ENABLE_INPUT_SPEECH -D ENABLE_NOTIFICATIONS -D ENABLE_GPU=1 -D ENABLE_EGLIMAGE=1 -D __STD_C -D _CRT_SECURE_NO_DEPRECATE -D _SCL_SECURE_NO_DEPRECATE -D NTDDI_VERSION=0x06020000 -D ENABLE_TASK_MANAGER=1 -D ENABLE_EXTENSIONS=1 -D ENABLE_PLUGIN_INSTALLATION=1 -D ENABLE_PLUGINS=1 -D ENABLE_SESSION_SERVICE=1 -D ENABLE_THEMES=1 -D ENABLE_AUTOFILL_DIALOG=1 -D ENABLE_BACKGROUND=1 -D ENABLE_AUTOMATION=1 -D ENABLE_GOOGLE_NOW=1 -D CLD_VERSION=1 -D ENABLE_FULL_PRINTING=1 -D ENABLE_PRINTING=1 -D ENABLE_SPELLCHECK=1 -D ENABLE_CAPTIVE_PORTAL_DETECTION=1 -D ENABLE_APP_LIST=1 -D ENABLE_SETTINGS_APP=1 -D ENABLE_MANAGED_USERS=1 -D ENABLE_MDNS=1 -D USING_CEF_SHARED -D __STDC_CONSTANT_MACROS -D __STDC_FORMAT_MACROS -D NDEBUG -D NVALGRIND -D DYNAMIC_ANNOTATIONS_ENABLED=0 
Release_Preprocessor_Definitions=-D ANGLE_DX11 -D _WIN32_WINNT=0x0602 -D WINVER=0x0602 -D GCC_BUILD -D _WINDOWS -D NOMINMAX -D PSAPI_VERSION=1 -D _CRT_RAND_S -D CERT_CHAIN_PARA_HAS_EXTRA_FIELDS -D WIN32_LEAN_AND_MEAN -D _ATL_NO_OPENGL -D _HAS_EXCEPTIONS=0 -D _SECURE_ATL -D CHROMIUM_BUILD -D TOOLKIT_VIEWS=1 -D USE_LIBJPEG_TURBO=1 -D ENABLE_ONE_CLICK_SIGNIN -D ENABLE_REMOTING=1 -D ENABLE_WEBRTC=1 -D ENABLE_PEPPER_CDMS -D ENABLE_CONFIGURATION_POLICY -D ENABLE_INPUT_SPEECH -D ENABLE_NOTIFICATIONS -D ENABLE_GPU=1 -D ENABLE_EGLIMAGE=1 -D __STD_C -D _CRT_SECURE_NO_DEPRECATE -D _SCL_SECURE_NO_DEPRECATE -D NTDDI_VERSION=0x06020000 -D ENABLE_TASK_MANAGER=1 -D ENABLE_EXTENSIONS=1 -D ENABLE_PLUGIN_INSTALLATION=1 -D ENABLE_PLUGINS=1 -D ENABLE_SESSION_SERVICE=1 -D ENABLE_THEMES=1 -D ENABLE_AUTOFILL_DIALOG=1 -D ENABLE_BACKGROUND=1 -D ENABLE_AUTOMATION=1 -D ENABLE_GOOGLE_NOW=1 -D CLD_VERSION=1 -D ENABLE_FULL_PRINTING=1 -D ENABLE_PRINTING=1 -D ENABLE_SPELLCHECK=1 -D ENABLE_CAPTIVE_PORTAL_DETECTION=1 -D ENABLE_APP_LIST=1 -D ENABLE_SETTINGS_APP=1 -D ENABLE_MANAGED_USERS=1 -D ENABLE_MDNS=1 -D USING_CEF_SHARED -D __STDC_CONSTANT_MACROS -D __STDC_FORMAT_MACROS -D NDEBUG -D NVALGRIND -D DYNAMIC_ANNOTATIONS_ENABLED=0 

# Implictly linked object files...
Debug_Implicitly_Linked_Objects=
Debug_Implicitly_Linked_Objects=
Release_Implicitly_Linked_Objects=
Release_Implicitly_Linked_Objects=

# Compiler flags...
Debug_Compiler_Flags=-Wall  -O0 -g 
Debug_Compiler_Flags=-Wall  -O0 -g 
Release_Compiler_Flags=-Wall -Werror -O2 
Release_Compiler_Flags=-Wall -Werror -O2 

# Builds all configurations for this project...
.PHONY: build_all_configurations
build_all_configurations: Debug Debug Release Release 

# Builds the Debug configuration...
.PHONY: Debug
Debug: out\Debug\obj\gcccefclient\cefclient\performance_test_tests.o out\Debug\obj\gcccefclient\cefclient\cefclient_win.o out\Debug\obj\gcccefclient\cefclient\window_test_win.o out\Debug\obj\gcccefclient\cefclient\client_switches.o out\Debug\obj\gcccefclient\cefclient\osrenderer.o out\Debug\obj\gcccefclient\cefclient\dom_test.o out\Debug\obj\gcccefclient\cefclient\dialog_test.o out\Debug\obj\gcccefclient\cefclient\client_renderer.o out\Debug\obj\gcccefclient\cefclient\client_app.o out\Debug\obj\gcccefclient\cefclient\window_test.o out\Debug\obj\gcccefclient\cefclient\cefclient_osr_widget_win.o out\Debug\obj\gcccefclient\cefclient\cefclient.o out\Debug\obj\gcccefclient\cefclient\string_util.o out\Debug\obj\gcccefclient\cefclient\client_app_delegates.o out\Debug\obj\gcccefclient\cefclient\performance_test.o out\Debug\obj\gcccefclient\cefclient\client_handler.o out\Debug\obj\gcccefclient\cefclient\scheme_test.o out\Debug\obj\gcccefclient\cefclient\resource_util_win.o out\Debug\obj\gcccefclient\cefclient\client_handler_win.o out\Debug\obj\gcccefclient\cefclient\binding_test.o 
	g++ out\Debug\obj\gcccefclient\cefclient\performance_test_tests.o out\Debug\obj\gcccefclient\cefclient\cefclient_win.o out\Debug\obj\gcccefclient\cefclient\window_test_win.o out\Debug\obj\gcccefclient\cefclient\client_switches.o out\Debug\obj\gcccefclient\cefclient\osrenderer.o out\Debug\obj\gcccefclient\cefclient\dom_test.o out\Debug\obj\gcccefclient\cefclient\dialog_test.o out\Debug\obj\gcccefclient\cefclient\client_renderer.o out\Debug\obj\gcccefclient\cefclient\client_app.o out\Debug\obj\gcccefclient\cefclient\window_test.o out\Debug\obj\gcccefclient\cefclient\cefclient_osr_widget_win.o out\Debug\obj\gcccefclient\cefclient\cefclient.o out\Debug\obj\gcccefclient\cefclient\string_util.o out\Debug\obj\gcccefclient\cefclient\client_app_delegates.o out\Debug\obj\gcccefclient\cefclient\performance_test.o out\Debug\obj\gcccefclient\cefclient\client_handler.o out\Debug\obj\gcccefclient\cefclient\scheme_test.o out\Debug\obj\gcccefclient\cefclient\resource_util_win.o out\Debug\obj\gcccefclient\cefclient\client_handler_win.o out\Debug\obj\gcccefclient\cefclient\binding_test.o  $(Debug_Library_Path) $(Debug_Libraries) -Wl,-rpath,.\ -o out\gccDebug\cefclient.exe

# Compiles file cefclient\performance_test_tests.cpp for the Debug configuration...
-include out\Debug\obj\gcccefclient\cefclient\performance_test_tests.d
out\Debug\obj\gcccefclient\cefclient\performance_test_tests.o: cefclient\performance_test_tests.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c cefclient\performance_test_tests.cpp $(Debug_Include_Path) -o out\Debug\obj\gcccefclient\cefclient\performance_test_tests.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM cefclient\performance_test_tests.cpp $(Debug_Include_Path) > out\Debug\obj\gcccefclient\cefclient\performance_test_tests.d

# Compiles file cefclient\cefclient_win.cpp for the Debug configuration...
-include out\Debug\obj\gcccefclient\cefclient\cefclient_win.d
out\Debug\obj\gcccefclient\cefclient\cefclient_win.o: cefclient\cefclient_win.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c cefclient\cefclient_win.cpp $(Debug_Include_Path) -o out\Debug\obj\gcccefclient\cefclient\cefclient_win.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM cefclient\cefclient_win.cpp $(Debug_Include_Path) > out\Debug\obj\gcccefclient\cefclient\cefclient_win.d

# Compiles file cefclient\window_test_win.cpp for the Debug configuration...
-include out\Debug\obj\gcccefclient\cefclient\window_test_win.d
out\Debug\obj\gcccefclient\cefclient\window_test_win.o: cefclient\window_test_win.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c cefclient\window_test_win.cpp $(Debug_Include_Path) -o out\Debug\obj\gcccefclient\cefclient\window_test_win.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM cefclient\window_test_win.cpp $(Debug_Include_Path) > out\Debug\obj\gcccefclient\cefclient\window_test_win.d

# Compiles file cefclient\client_switches.cpp for the Debug configuration...
-include out\Debug\obj\gcccefclient\cefclient\client_switches.d
out\Debug\obj\gcccefclient\cefclient\client_switches.o: cefclient\client_switches.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c cefclient\client_switches.cpp $(Debug_Include_Path) -o out\Debug\obj\gcccefclient\cefclient\client_switches.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM cefclient\client_switches.cpp $(Debug_Include_Path) > out\Debug\obj\gcccefclient\cefclient\client_switches.d

# Compiles file cefclient\osrenderer.cpp for the Debug configuration...
-include out\Debug\obj\gcccefclient\cefclient\osrenderer.d
out\Debug\obj\gcccefclient\cefclient\osrenderer.o: cefclient\osrenderer.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c cefclient\osrenderer.cpp $(Debug_Include_Path) -o out\Debug\obj\gcccefclient\cefclient\osrenderer.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM cefclient\osrenderer.cpp $(Debug_Include_Path) > out\Debug\obj\gcccefclient\cefclient\osrenderer.d

# Compiles file cefclient\dom_test.cpp for the Debug configuration...
-include out\Debug\obj\gcccefclient\cefclient\dom_test.d
out\Debug\obj\gcccefclient\cefclient\dom_test.o: cefclient\dom_test.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c cefclient\dom_test.cpp $(Debug_Include_Path) -o out\Debug\obj\gcccefclient\cefclient\dom_test.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM cefclient\dom_test.cpp $(Debug_Include_Path) > out\Debug\obj\gcccefclient\cefclient\dom_test.d

# Compiles file cefclient\dialog_test.cpp for the Debug configuration...
-include out\Debug\obj\gcccefclient\cefclient\dialog_test.d
out\Debug\obj\gcccefclient\cefclient\dialog_test.o: cefclient\dialog_test.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c cefclient\dialog_test.cpp $(Debug_Include_Path) -o out\Debug\obj\gcccefclient\cefclient\dialog_test.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM cefclient\dialog_test.cpp $(Debug_Include_Path) > out\Debug\obj\gcccefclient\cefclient\dialog_test.d

# Compiles file cefclient\client_renderer.cpp for the Debug configuration...
-include out\Debug\obj\gcccefclient\cefclient\client_renderer.d
out\Debug\obj\gcccefclient\cefclient\client_renderer.o: cefclient\client_renderer.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c cefclient\client_renderer.cpp $(Debug_Include_Path) -o out\Debug\obj\gcccefclient\cefclient\client_renderer.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM cefclient\client_renderer.cpp $(Debug_Include_Path) > out\Debug\obj\gcccefclient\cefclient\client_renderer.d

# Compiles file cefclient\client_app.cpp for the Debug configuration...
-include out\Debug\obj\gcccefclient\cefclient\client_app.d
out\Debug\obj\gcccefclient\cefclient\client_app.o: cefclient\client_app.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c cefclient\client_app.cpp $(Debug_Include_Path) -o out\Debug\obj\gcccefclient\cefclient\client_app.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM cefclient\client_app.cpp $(Debug_Include_Path) > out\Debug\obj\gcccefclient\cefclient\client_app.d

# Compiles file cefclient\window_test.cpp for the Debug configuration...
-include out\Debug\obj\gcccefclient\cefclient\window_test.d
out\Debug\obj\gcccefclient\cefclient\window_test.o: cefclient\window_test.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c cefclient\window_test.cpp $(Debug_Include_Path) -o out\Debug\obj\gcccefclient\cefclient\window_test.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM cefclient\window_test.cpp $(Debug_Include_Path) > out\Debug\obj\gcccefclient\cefclient\window_test.d

# Compiles file cefclient\cefclient_osr_widget_win.cpp for the Debug configuration...
-include out\Debug\obj\gcccefclient\cefclient\cefclient_osr_widget_win.d
out\Debug\obj\gcccefclient\cefclient\cefclient_osr_widget_win.o: cefclient\cefclient_osr_widget_win.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c cefclient\cefclient_osr_widget_win.cpp $(Debug_Include_Path) -o out\Debug\obj\gcccefclient\cefclient\cefclient_osr_widget_win.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM cefclient\cefclient_osr_widget_win.cpp $(Debug_Include_Path) > out\Debug\obj\gcccefclient\cefclient\cefclient_osr_widget_win.d

# Compiles file cefclient\cefclient.cpp for the Debug configuration...
-include out\Debug\obj\gcccefclient\cefclient\cefclient.d
out\Debug\obj\gcccefclient\cefclient\cefclient.o: cefclient\cefclient.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c cefclient\cefclient.cpp $(Debug_Include_Path) -o out\Debug\obj\gcccefclient\cefclient\cefclient.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM cefclient\cefclient.cpp $(Debug_Include_Path) > out\Debug\obj\gcccefclient\cefclient\cefclient.d

# Compiles file cefclient\string_util.cpp for the Debug configuration...
-include out\Debug\obj\gcccefclient\cefclient\string_util.d
out\Debug\obj\gcccefclient\cefclient\string_util.o: cefclient\string_util.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c cefclient\string_util.cpp $(Debug_Include_Path) -o out\Debug\obj\gcccefclient\cefclient\string_util.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM cefclient\string_util.cpp $(Debug_Include_Path) > out\Debug\obj\gcccefclient\cefclient\string_util.d

# Compiles file cefclient\client_app_delegates.cpp for the Debug configuration...
-include out\Debug\obj\gcccefclient\cefclient\client_app_delegates.d
out\Debug\obj\gcccefclient\cefclient\client_app_delegates.o: cefclient\client_app_delegates.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c cefclient\client_app_delegates.cpp $(Debug_Include_Path) -o out\Debug\obj\gcccefclient\cefclient\client_app_delegates.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM cefclient\client_app_delegates.cpp $(Debug_Include_Path) > out\Debug\obj\gcccefclient\cefclient\client_app_delegates.d

# Compiles file cefclient\performance_test.cpp for the Debug configuration...
-include out\Debug\obj\gcccefclient\cefclient\performance_test.d
out\Debug\obj\gcccefclient\cefclient\performance_test.o: cefclient\performance_test.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c cefclient\performance_test.cpp $(Debug_Include_Path) -o out\Debug\obj\gcccefclient\cefclient\performance_test.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM cefclient\performance_test.cpp $(Debug_Include_Path) > out\Debug\obj\gcccefclient\cefclient\performance_test.d

# Compiles file cefclient\client_handler.cpp for the Debug configuration...
-include out\Debug\obj\gcccefclient\cefclient\client_handler.d
out\Debug\obj\gcccefclient\cefclient\client_handler.o: cefclient\client_handler.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c cefclient\client_handler.cpp $(Debug_Include_Path) -o out\Debug\obj\gcccefclient\cefclient\client_handler.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM cefclient\client_handler.cpp $(Debug_Include_Path) > out\Debug\obj\gcccefclient\cefclient\client_handler.d

# Compiles file cefclient\scheme_test.cpp for the Debug configuration...
-include out\Debug\obj\gcccefclient\cefclient\scheme_test.d
out\Debug\obj\gcccefclient\cefclient\scheme_test.o: cefclient\scheme_test.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c cefclient\scheme_test.cpp $(Debug_Include_Path) -o out\Debug\obj\gcccefclient\cefclient\scheme_test.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM cefclient\scheme_test.cpp $(Debug_Include_Path) > out\Debug\obj\gcccefclient\cefclient\scheme_test.d

# Compiles file cefclient\resource_util_win.cpp for the Debug configuration...
-include out\Debug\obj\gcccefclient\cefclient\resource_util_win.d
out\Debug\obj\gcccefclient\cefclient\resource_util_win.o: cefclient\resource_util_win.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c cefclient\resource_util_win.cpp $(Debug_Include_Path) -o out\Debug\obj\gcccefclient\cefclient\resource_util_win.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM cefclient\resource_util_win.cpp $(Debug_Include_Path) > out\Debug\obj\gcccefclient\cefclient\resource_util_win.d

# Compiles file cefclient\client_handler_win.cpp for the Debug configuration...
-include out\Debug\obj\gcccefclient\cefclient\client_handler_win.d
out\Debug\obj\gcccefclient\cefclient\client_handler_win.o: cefclient\client_handler_win.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c cefclient\client_handler_win.cpp $(Debug_Include_Path) -o out\Debug\obj\gcccefclient\cefclient\client_handler_win.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM cefclient\client_handler_win.cpp $(Debug_Include_Path) > out\Debug\obj\gcccefclient\cefclient\client_handler_win.d

# Compiles file cefclient\binding_test.cpp for the Debug configuration...
-include out\Debug\obj\gcccefclient\cefclient\binding_test.d
out\Debug\obj\gcccefclient\cefclient\binding_test.o: cefclient\binding_test.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c cefclient\binding_test.cpp $(Debug_Include_Path) -o out\Debug\obj\gcccefclient\cefclient\binding_test.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM cefclient\binding_test.cpp $(Debug_Include_Path) > out\Debug\obj\gcccefclient\cefclient\binding_test.d

# Builds the Debug configuration...
.PHONY: Debug
Debug: out\Debug\obj\gcccefclient\cefclient\performance_test_tests.o out\Debug\obj\gcccefclient\cefclient\cefclient_win.o out\Debug\obj\gcccefclient\cefclient\window_test_win.o out\Debug\obj\gcccefclient\cefclient\client_switches.o out\Debug\obj\gcccefclient\cefclient\osrenderer.o out\Debug\obj\gcccefclient\cefclient\dom_test.o out\Debug\obj\gcccefclient\cefclient\dialog_test.o out\Debug\obj\gcccefclient\cefclient\client_renderer.o out\Debug\obj\gcccefclient\cefclient\client_app.o out\Debug\obj\gcccefclient\cefclient\window_test.o out\Debug\obj\gcccefclient\cefclient\cefclient_osr_widget_win.o out\Debug\obj\gcccefclient\cefclient\cefclient.o out\Debug\obj\gcccefclient\cefclient\string_util.o out\Debug\obj\gcccefclient\cefclient\client_app_delegates.o out\Debug\obj\gcccefclient\cefclient\performance_test.o out\Debug\obj\gcccefclient\cefclient\client_handler.o out\Debug\obj\gcccefclient\cefclient\scheme_test.o out\Debug\obj\gcccefclient\cefclient\resource_util_win.o out\Debug\obj\gcccefclient\cefclient\client_handler_win.o out\Debug\obj\gcccefclient\cefclient\binding_test.o 
	g++ out\Debug\obj\gcccefclient\cefclient\performance_test_tests.o out\Debug\obj\gcccefclient\cefclient\cefclient_win.o out\Debug\obj\gcccefclient\cefclient\window_test_win.o out\Debug\obj\gcccefclient\cefclient\client_switches.o out\Debug\obj\gcccefclient\cefclient\osrenderer.o out\Debug\obj\gcccefclient\cefclient\dom_test.o out\Debug\obj\gcccefclient\cefclient\dialog_test.o out\Debug\obj\gcccefclient\cefclient\client_renderer.o out\Debug\obj\gcccefclient\cefclient\client_app.o out\Debug\obj\gcccefclient\cefclient\window_test.o out\Debug\obj\gcccefclient\cefclient\cefclient_osr_widget_win.o out\Debug\obj\gcccefclient\cefclient\cefclient.o out\Debug\obj\gcccefclient\cefclient\string_util.o out\Debug\obj\gcccefclient\cefclient\client_app_delegates.o out\Debug\obj\gcccefclient\cefclient\performance_test.o out\Debug\obj\gcccefclient\cefclient\client_handler.o out\Debug\obj\gcccefclient\cefclient\scheme_test.o out\Debug\obj\gcccefclient\cefclient\resource_util_win.o out\Debug\obj\gcccefclient\cefclient\client_handler_win.o out\Debug\obj\gcccefclient\cefclient\binding_test.o  $(Debug_Library_Path) $(Debug_Libraries) -Wl,-rpath,.\ -o out\gccDebug\cefclient.exe

# Compiles file cefclient\performance_test_tests.cpp for the Debug configuration...
-include out\Debug\obj\gcccefclient\cefclient\performance_test_tests.d
out\Debug\obj\gcccefclient\cefclient\performance_test_tests.o: cefclient\performance_test_tests.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c cefclient\performance_test_tests.cpp $(Debug_Include_Path) -o out\Debug\obj\gcccefclient\cefclient\performance_test_tests.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM cefclient\performance_test_tests.cpp $(Debug_Include_Path) > out\Debug\obj\gcccefclient\cefclient\performance_test_tests.d

# Compiles file cefclient\cefclient_win.cpp for the Debug configuration...
-include out\Debug\obj\gcccefclient\cefclient\cefclient_win.d
out\Debug\obj\gcccefclient\cefclient\cefclient_win.o: cefclient\cefclient_win.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c cefclient\cefclient_win.cpp $(Debug_Include_Path) -o out\Debug\obj\gcccefclient\cefclient\cefclient_win.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM cefclient\cefclient_win.cpp $(Debug_Include_Path) > out\Debug\obj\gcccefclient\cefclient\cefclient_win.d

# Compiles file cefclient\window_test_win.cpp for the Debug configuration...
-include out\Debug\obj\gcccefclient\cefclient\window_test_win.d
out\Debug\obj\gcccefclient\cefclient\window_test_win.o: cefclient\window_test_win.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c cefclient\window_test_win.cpp $(Debug_Include_Path) -o out\Debug\obj\gcccefclient\cefclient\window_test_win.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM cefclient\window_test_win.cpp $(Debug_Include_Path) > out\Debug\obj\gcccefclient\cefclient\window_test_win.d

# Compiles file cefclient\client_switches.cpp for the Debug configuration...
-include out\Debug\obj\gcccefclient\cefclient\client_switches.d
out\Debug\obj\gcccefclient\cefclient\client_switches.o: cefclient\client_switches.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c cefclient\client_switches.cpp $(Debug_Include_Path) -o out\Debug\obj\gcccefclient\cefclient\client_switches.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM cefclient\client_switches.cpp $(Debug_Include_Path) > out\Debug\obj\gcccefclient\cefclient\client_switches.d

# Compiles file cefclient\osrenderer.cpp for the Debug configuration...
-include out\Debug\obj\gcccefclient\cefclient\osrenderer.d
out\Debug\obj\gcccefclient\cefclient\osrenderer.o: cefclient\osrenderer.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c cefclient\osrenderer.cpp $(Debug_Include_Path) -o out\Debug\obj\gcccefclient\cefclient\osrenderer.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM cefclient\osrenderer.cpp $(Debug_Include_Path) > out\Debug\obj\gcccefclient\cefclient\osrenderer.d

# Compiles file cefclient\dom_test.cpp for the Debug configuration...
-include out\Debug\obj\gcccefclient\cefclient\dom_test.d
out\Debug\obj\gcccefclient\cefclient\dom_test.o: cefclient\dom_test.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c cefclient\dom_test.cpp $(Debug_Include_Path) -o out\Debug\obj\gcccefclient\cefclient\dom_test.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM cefclient\dom_test.cpp $(Debug_Include_Path) > out\Debug\obj\gcccefclient\cefclient\dom_test.d

# Compiles file cefclient\dialog_test.cpp for the Debug configuration...
-include out\Debug\obj\gcccefclient\cefclient\dialog_test.d
out\Debug\obj\gcccefclient\cefclient\dialog_test.o: cefclient\dialog_test.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c cefclient\dialog_test.cpp $(Debug_Include_Path) -o out\Debug\obj\gcccefclient\cefclient\dialog_test.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM cefclient\dialog_test.cpp $(Debug_Include_Path) > out\Debug\obj\gcccefclient\cefclient\dialog_test.d

# Compiles file cefclient\client_renderer.cpp for the Debug configuration...
-include out\Debug\obj\gcccefclient\cefclient\client_renderer.d
out\Debug\obj\gcccefclient\cefclient\client_renderer.o: cefclient\client_renderer.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c cefclient\client_renderer.cpp $(Debug_Include_Path) -o out\Debug\obj\gcccefclient\cefclient\client_renderer.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM cefclient\client_renderer.cpp $(Debug_Include_Path) > out\Debug\obj\gcccefclient\cefclient\client_renderer.d

# Compiles file cefclient\client_app.cpp for the Debug configuration...
-include out\Debug\obj\gcccefclient\cefclient\client_app.d
out\Debug\obj\gcccefclient\cefclient\client_app.o: cefclient\client_app.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c cefclient\client_app.cpp $(Debug_Include_Path) -o out\Debug\obj\gcccefclient\cefclient\client_app.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM cefclient\client_app.cpp $(Debug_Include_Path) > out\Debug\obj\gcccefclient\cefclient\client_app.d

# Compiles file cefclient\window_test.cpp for the Debug configuration...
-include out\Debug\obj\gcccefclient\cefclient\window_test.d
out\Debug\obj\gcccefclient\cefclient\window_test.o: cefclient\window_test.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c cefclient\window_test.cpp $(Debug_Include_Path) -o out\Debug\obj\gcccefclient\cefclient\window_test.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM cefclient\window_test.cpp $(Debug_Include_Path) > out\Debug\obj\gcccefclient\cefclient\window_test.d

# Compiles file cefclient\cefclient_osr_widget_win.cpp for the Debug configuration...
-include out\Debug\obj\gcccefclient\cefclient\cefclient_osr_widget_win.d
out\Debug\obj\gcccefclient\cefclient\cefclient_osr_widget_win.o: cefclient\cefclient_osr_widget_win.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c cefclient\cefclient_osr_widget_win.cpp $(Debug_Include_Path) -o out\Debug\obj\gcccefclient\cefclient\cefclient_osr_widget_win.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM cefclient\cefclient_osr_widget_win.cpp $(Debug_Include_Path) > out\Debug\obj\gcccefclient\cefclient\cefclient_osr_widget_win.d

# Compiles file cefclient\cefclient.cpp for the Debug configuration...
-include out\Debug\obj\gcccefclient\cefclient\cefclient.d
out\Debug\obj\gcccefclient\cefclient\cefclient.o: cefclient\cefclient.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c cefclient\cefclient.cpp $(Debug_Include_Path) -o out\Debug\obj\gcccefclient\cefclient\cefclient.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM cefclient\cefclient.cpp $(Debug_Include_Path) > out\Debug\obj\gcccefclient\cefclient\cefclient.d

# Compiles file cefclient\string_util.cpp for the Debug configuration...
-include out\Debug\obj\gcccefclient\cefclient\string_util.d
out\Debug\obj\gcccefclient\cefclient\string_util.o: cefclient\string_util.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c cefclient\string_util.cpp $(Debug_Include_Path) -o out\Debug\obj\gcccefclient\cefclient\string_util.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM cefclient\string_util.cpp $(Debug_Include_Path) > out\Debug\obj\gcccefclient\cefclient\string_util.d

# Compiles file cefclient\client_app_delegates.cpp for the Debug configuration...
-include out\Debug\obj\gcccefclient\cefclient\client_app_delegates.d
out\Debug\obj\gcccefclient\cefclient\client_app_delegates.o: cefclient\client_app_delegates.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c cefclient\client_app_delegates.cpp $(Debug_Include_Path) -o out\Debug\obj\gcccefclient\cefclient\client_app_delegates.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM cefclient\client_app_delegates.cpp $(Debug_Include_Path) > out\Debug\obj\gcccefclient\cefclient\client_app_delegates.d

# Compiles file cefclient\performance_test.cpp for the Debug configuration...
-include out\Debug\obj\gcccefclient\cefclient\performance_test.d
out\Debug\obj\gcccefclient\cefclient\performance_test.o: cefclient\performance_test.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c cefclient\performance_test.cpp $(Debug_Include_Path) -o out\Debug\obj\gcccefclient\cefclient\performance_test.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM cefclient\performance_test.cpp $(Debug_Include_Path) > out\Debug\obj\gcccefclient\cefclient\performance_test.d

# Compiles file cefclient\client_handler.cpp for the Debug configuration...
-include out\Debug\obj\gcccefclient\cefclient\client_handler.d
out\Debug\obj\gcccefclient\cefclient\client_handler.o: cefclient\client_handler.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c cefclient\client_handler.cpp $(Debug_Include_Path) -o out\Debug\obj\gcccefclient\cefclient\client_handler.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM cefclient\client_handler.cpp $(Debug_Include_Path) > out\Debug\obj\gcccefclient\cefclient\client_handler.d

# Compiles file cefclient\scheme_test.cpp for the Debug configuration...
-include out\Debug\obj\gcccefclient\cefclient\scheme_test.d
out\Debug\obj\gcccefclient\cefclient\scheme_test.o: cefclient\scheme_test.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c cefclient\scheme_test.cpp $(Debug_Include_Path) -o out\Debug\obj\gcccefclient\cefclient\scheme_test.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM cefclient\scheme_test.cpp $(Debug_Include_Path) > out\Debug\obj\gcccefclient\cefclient\scheme_test.d

# Compiles file cefclient\resource_util_win.cpp for the Debug configuration...
-include out\Debug\obj\gcccefclient\cefclient\resource_util_win.d
out\Debug\obj\gcccefclient\cefclient\resource_util_win.o: cefclient\resource_util_win.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c cefclient\resource_util_win.cpp $(Debug_Include_Path) -o out\Debug\obj\gcccefclient\cefclient\resource_util_win.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM cefclient\resource_util_win.cpp $(Debug_Include_Path) > out\Debug\obj\gcccefclient\cefclient\resource_util_win.d

# Compiles file cefclient\client_handler_win.cpp for the Debug configuration...
-include out\Debug\obj\gcccefclient\cefclient\client_handler_win.d
out\Debug\obj\gcccefclient\cefclient\client_handler_win.o: cefclient\client_handler_win.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c cefclient\client_handler_win.cpp $(Debug_Include_Path) -o out\Debug\obj\gcccefclient\cefclient\client_handler_win.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM cefclient\client_handler_win.cpp $(Debug_Include_Path) > out\Debug\obj\gcccefclient\cefclient\client_handler_win.d

# Compiles file cefclient\binding_test.cpp for the Debug configuration...
-include out\Debug\obj\gcccefclient\cefclient\binding_test.d
out\Debug\obj\gcccefclient\cefclient\binding_test.o: cefclient\binding_test.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c cefclient\binding_test.cpp $(Debug_Include_Path) -o out\Debug\obj\gcccefclient\cefclient\binding_test.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM cefclient\binding_test.cpp $(Debug_Include_Path) > out\Debug\obj\gcccefclient\cefclient\binding_test.d

# Builds the Release configuration...
.PHONY: Release
Release: create_folders out\Release\obj\gcccefclient\cefclient\performance_test_tests.o out\Release\obj\gcccefclient\cefclient\cefclient_win.o out\Release\obj\gcccefclient\cefclient\window_test_win.o out\Release\obj\gcccefclient\cefclient\client_switches.o out\Release\obj\gcccefclient\cefclient\osrenderer.o out\Release\obj\gcccefclient\cefclient\dom_test.o out\Release\obj\gcccefclient\cefclient\dialog_test.o out\Release\obj\gcccefclient\cefclient\client_renderer.o out\Release\obj\gcccefclient\cefclient\client_app.o out\Release\obj\gcccefclient\cefclient\window_test.o out\Release\obj\gcccefclient\cefclient\cefclient_osr_widget_win.o out\Release\obj\gcccefclient\cefclient\cefclient.o out\Release\obj\gcccefclient\cefclient\string_util.o out\Release\obj\gcccefclient\cefclient\client_app_delegates.o out\Release\obj\gcccefclient\cefclient\performance_test.o out\Release\obj\gcccefclient\cefclient\client_handler.o out\Release\obj\gcccefclient\cefclient\scheme_test.o out\Release\obj\gcccefclient\cefclient\resource_util_win.o out\Release\obj\gcccefclient\cefclient\client_handler_win.o out\Release\obj\gcccefclient\cefclient\binding_test.o 
	g++ out\Release\obj\gcccefclient\cefclient\performance_test_tests.o out\Release\obj\gcccefclient\cefclient\cefclient_win.o out\Release\obj\gcccefclient\cefclient\window_test_win.o out\Release\obj\gcccefclient\cefclient\client_switches.o out\Release\obj\gcccefclient\cefclient\osrenderer.o out\Release\obj\gcccefclient\cefclient\dom_test.o out\Release\obj\gcccefclient\cefclient\dialog_test.o out\Release\obj\gcccefclient\cefclient\client_renderer.o out\Release\obj\gcccefclient\cefclient\client_app.o out\Release\obj\gcccefclient\cefclient\window_test.o out\Release\obj\gcccefclient\cefclient\cefclient_osr_widget_win.o out\Release\obj\gcccefclient\cefclient\cefclient.o out\Release\obj\gcccefclient\cefclient\string_util.o out\Release\obj\gcccefclient\cefclient\client_app_delegates.o out\Release\obj\gcccefclient\cefclient\performance_test.o out\Release\obj\gcccefclient\cefclient\client_handler.o out\Release\obj\gcccefclient\cefclient\scheme_test.o out\Release\obj\gcccefclient\cefclient\resource_util_win.o out\Release\obj\gcccefclient\cefclient\client_handler_win.o out\Release\obj\gcccefclient\cefclient\binding_test.o  $(Release_Library_Path) $(Release_Libraries) -Wl,-rpath,.\ -o out\gccRelease\cefclient.exe

# Compiles file cefclient\performance_test_tests.cpp for the Release configuration...
-include out\Release\obj\gcccefclient\cefclient\performance_test_tests.d
out\Release\obj\gcccefclient\cefclient\performance_test_tests.o: cefclient\performance_test_tests.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c cefclient\performance_test_tests.cpp $(Release_Include_Path) -o out\Release\obj\gcccefclient\cefclient\performance_test_tests.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM cefclient\performance_test_tests.cpp $(Release_Include_Path) > out\Release\obj\gcccefclient\cefclient\performance_test_tests.d

# Compiles file cefclient\cefclient_win.cpp for the Release configuration...
-include out\Release\obj\gcccefclient\cefclient\cefclient_win.d
out\Release\obj\gcccefclient\cefclient\cefclient_win.o: cefclient\cefclient_win.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c cefclient\cefclient_win.cpp $(Release_Include_Path) -o out\Release\obj\gcccefclient\cefclient\cefclient_win.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM cefclient\cefclient_win.cpp $(Release_Include_Path) > out\Release\obj\gcccefclient\cefclient\cefclient_win.d

# Compiles file cefclient\window_test_win.cpp for the Release configuration...
-include out\Release\obj\gcccefclient\cefclient\window_test_win.d
out\Release\obj\gcccefclient\cefclient\window_test_win.o: cefclient\window_test_win.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c cefclient\window_test_win.cpp $(Release_Include_Path) -o out\Release\obj\gcccefclient\cefclient\window_test_win.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM cefclient\window_test_win.cpp $(Release_Include_Path) > out\Release\obj\gcccefclient\cefclient\window_test_win.d

# Compiles file cefclient\client_switches.cpp for the Release configuration...
-include out\Release\obj\gcccefclient\cefclient\client_switches.d
out\Release\obj\gcccefclient\cefclient\client_switches.o: cefclient\client_switches.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c cefclient\client_switches.cpp $(Release_Include_Path) -o out\Release\obj\gcccefclient\cefclient\client_switches.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM cefclient\client_switches.cpp $(Release_Include_Path) > out\Release\obj\gcccefclient\cefclient\client_switches.d

# Compiles file cefclient\osrenderer.cpp for the Release configuration...
-include out\Release\obj\gcccefclient\cefclient\osrenderer.d
out\Release\obj\gcccefclient\cefclient\osrenderer.o: cefclient\osrenderer.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c cefclient\osrenderer.cpp $(Release_Include_Path) -o out\Release\obj\gcccefclient\cefclient\osrenderer.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM cefclient\osrenderer.cpp $(Release_Include_Path) > out\Release\obj\gcccefclient\cefclient\osrenderer.d

# Compiles file cefclient\dom_test.cpp for the Release configuration...
-include out\Release\obj\gcccefclient\cefclient\dom_test.d
out\Release\obj\gcccefclient\cefclient\dom_test.o: cefclient\dom_test.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c cefclient\dom_test.cpp $(Release_Include_Path) -o out\Release\obj\gcccefclient\cefclient\dom_test.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM cefclient\dom_test.cpp $(Release_Include_Path) > out\Release\obj\gcccefclient\cefclient\dom_test.d

# Compiles file cefclient\dialog_test.cpp for the Release configuration...
-include out\Release\obj\gcccefclient\cefclient\dialog_test.d
out\Release\obj\gcccefclient\cefclient\dialog_test.o: cefclient\dialog_test.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c cefclient\dialog_test.cpp $(Release_Include_Path) -o out\Release\obj\gcccefclient\cefclient\dialog_test.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM cefclient\dialog_test.cpp $(Release_Include_Path) > out\Release\obj\gcccefclient\cefclient\dialog_test.d

# Compiles file cefclient\client_renderer.cpp for the Release configuration...
-include out\Release\obj\gcccefclient\cefclient\client_renderer.d
out\Release\obj\gcccefclient\cefclient\client_renderer.o: cefclient\client_renderer.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c cefclient\client_renderer.cpp $(Release_Include_Path) -o out\Release\obj\gcccefclient\cefclient\client_renderer.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM cefclient\client_renderer.cpp $(Release_Include_Path) > out\Release\obj\gcccefclient\cefclient\client_renderer.d

# Compiles file cefclient\client_app.cpp for the Release configuration...
-include out\Release\obj\gcccefclient\cefclient\client_app.d
out\Release\obj\gcccefclient\cefclient\client_app.o: cefclient\client_app.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c cefclient\client_app.cpp $(Release_Include_Path) -o out\Release\obj\gcccefclient\cefclient\client_app.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM cefclient\client_app.cpp $(Release_Include_Path) > out\Release\obj\gcccefclient\cefclient\client_app.d

# Compiles file cefclient\window_test.cpp for the Release configuration...
-include out\Release\obj\gcccefclient\cefclient\window_test.d
out\Release\obj\gcccefclient\cefclient\window_test.o: cefclient\window_test.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c cefclient\window_test.cpp $(Release_Include_Path) -o out\Release\obj\gcccefclient\cefclient\window_test.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM cefclient\window_test.cpp $(Release_Include_Path) > out\Release\obj\gcccefclient\cefclient\window_test.d

# Compiles file cefclient\cefclient_osr_widget_win.cpp for the Release configuration...
-include out\Release\obj\gcccefclient\cefclient\cefclient_osr_widget_win.d
out\Release\obj\gcccefclient\cefclient\cefclient_osr_widget_win.o: cefclient\cefclient_osr_widget_win.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c cefclient\cefclient_osr_widget_win.cpp $(Release_Include_Path) -o out\Release\obj\gcccefclient\cefclient\cefclient_osr_widget_win.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM cefclient\cefclient_osr_widget_win.cpp $(Release_Include_Path) > out\Release\obj\gcccefclient\cefclient\cefclient_osr_widget_win.d

# Compiles file cefclient\cefclient.cpp for the Release configuration...
-include out\Release\obj\gcccefclient\cefclient\cefclient.d
out\Release\obj\gcccefclient\cefclient\cefclient.o: cefclient\cefclient.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c cefclient\cefclient.cpp $(Release_Include_Path) -o out\Release\obj\gcccefclient\cefclient\cefclient.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM cefclient\cefclient.cpp $(Release_Include_Path) > out\Release\obj\gcccefclient\cefclient\cefclient.d

# Compiles file cefclient\string_util.cpp for the Release configuration...
-include out\Release\obj\gcccefclient\cefclient\string_util.d
out\Release\obj\gcccefclient\cefclient\string_util.o: cefclient\string_util.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c cefclient\string_util.cpp $(Release_Include_Path) -o out\Release\obj\gcccefclient\cefclient\string_util.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM cefclient\string_util.cpp $(Release_Include_Path) > out\Release\obj\gcccefclient\cefclient\string_util.d

# Compiles file cefclient\client_app_delegates.cpp for the Release configuration...
-include out\Release\obj\gcccefclient\cefclient\client_app_delegates.d
out\Release\obj\gcccefclient\cefclient\client_app_delegates.o: cefclient\client_app_delegates.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c cefclient\client_app_delegates.cpp $(Release_Include_Path) -o out\Release\obj\gcccefclient\cefclient\client_app_delegates.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM cefclient\client_app_delegates.cpp $(Release_Include_Path) > out\Release\obj\gcccefclient\cefclient\client_app_delegates.d

# Compiles file cefclient\performance_test.cpp for the Release configuration...
-include out\Release\obj\gcccefclient\cefclient\performance_test.d
out\Release\obj\gcccefclient\cefclient\performance_test.o: cefclient\performance_test.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c cefclient\performance_test.cpp $(Release_Include_Path) -o out\Release\obj\gcccefclient\cefclient\performance_test.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM cefclient\performance_test.cpp $(Release_Include_Path) > out\Release\obj\gcccefclient\cefclient\performance_test.d

# Compiles file cefclient\client_handler.cpp for the Release configuration...
-include out\Release\obj\gcccefclient\cefclient\client_handler.d
out\Release\obj\gcccefclient\cefclient\client_handler.o: cefclient\client_handler.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c cefclient\client_handler.cpp $(Release_Include_Path) -o out\Release\obj\gcccefclient\cefclient\client_handler.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM cefclient\client_handler.cpp $(Release_Include_Path) > out\Release\obj\gcccefclient\cefclient\client_handler.d

# Compiles file cefclient\scheme_test.cpp for the Release configuration...
-include out\Release\obj\gcccefclient\cefclient\scheme_test.d
out\Release\obj\gcccefclient\cefclient\scheme_test.o: cefclient\scheme_test.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c cefclient\scheme_test.cpp $(Release_Include_Path) -o out\Release\obj\gcccefclient\cefclient\scheme_test.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM cefclient\scheme_test.cpp $(Release_Include_Path) > out\Release\obj\gcccefclient\cefclient\scheme_test.d

# Compiles file cefclient\resource_util_win.cpp for the Release configuration...
-include out\Release\obj\gcccefclient\cefclient\resource_util_win.d
out\Release\obj\gcccefclient\cefclient\resource_util_win.o: cefclient\resource_util_win.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c cefclient\resource_util_win.cpp $(Release_Include_Path) -o out\Release\obj\gcccefclient\cefclient\resource_util_win.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM cefclient\resource_util_win.cpp $(Release_Include_Path) > out\Release\obj\gcccefclient\cefclient\resource_util_win.d

# Compiles file cefclient\client_handler_win.cpp for the Release configuration...
-include out\Release\obj\gcccefclient\cefclient\client_handler_win.d
out\Release\obj\gcccefclient\cefclient\client_handler_win.o: cefclient\client_handler_win.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c cefclient\client_handler_win.cpp $(Release_Include_Path) -o out\Release\obj\gcccefclient\cefclient\client_handler_win.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM cefclient\client_handler_win.cpp $(Release_Include_Path) > out\Release\obj\gcccefclient\cefclient\client_handler_win.d

# Compiles file cefclient\binding_test.cpp for the Release configuration...
-include out\Release\obj\gcccefclient\cefclient\binding_test.d
out\Release\obj\gcccefclient\cefclient\binding_test.o: cefclient\binding_test.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c cefclient\binding_test.cpp $(Release_Include_Path) -o out\Release\obj\gcccefclient\cefclient\binding_test.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM cefclient\binding_test.cpp $(Release_Include_Path) > out\Release\obj\gcccefclient\cefclient\binding_test.d

# Builds the Release configuration...
.PHONY: Release
Release: create_folders out\Release\obj\gcccefclient\cefclient\performance_test_tests.o out\Release\obj\gcccefclient\cefclient\cefclient_win.o out\Release\obj\gcccefclient\cefclient\window_test_win.o out\Release\obj\gcccefclient\cefclient\client_switches.o out\Release\obj\gcccefclient\cefclient\osrenderer.o out\Release\obj\gcccefclient\cefclient\dom_test.o out\Release\obj\gcccefclient\cefclient\dialog_test.o out\Release\obj\gcccefclient\cefclient\client_renderer.o out\Release\obj\gcccefclient\cefclient\client_app.o out\Release\obj\gcccefclient\cefclient\window_test.o out\Release\obj\gcccefclient\cefclient\cefclient_osr_widget_win.o out\Release\obj\gcccefclient\cefclient\cefclient.o out\Release\obj\gcccefclient\cefclient\string_util.o out\Release\obj\gcccefclient\cefclient\client_app_delegates.o out\Release\obj\gcccefclient\cefclient\performance_test.o out\Release\obj\gcccefclient\cefclient\client_handler.o out\Release\obj\gcccefclient\cefclient\scheme_test.o out\Release\obj\gcccefclient\cefclient\resource_util_win.o out\Release\obj\gcccefclient\cefclient\client_handler_win.o out\Release\obj\gcccefclient\cefclient\binding_test.o 
	g++ out\Release\obj\gcccefclient\cefclient\performance_test_tests.o out\Release\obj\gcccefclient\cefclient\cefclient_win.o out\Release\obj\gcccefclient\cefclient\window_test_win.o out\Release\obj\gcccefclient\cefclient\client_switches.o out\Release\obj\gcccefclient\cefclient\osrenderer.o out\Release\obj\gcccefclient\cefclient\dom_test.o out\Release\obj\gcccefclient\cefclient\dialog_test.o out\Release\obj\gcccefclient\cefclient\client_renderer.o out\Release\obj\gcccefclient\cefclient\client_app.o out\Release\obj\gcccefclient\cefclient\window_test.o out\Release\obj\gcccefclient\cefclient\cefclient_osr_widget_win.o out\Release\obj\gcccefclient\cefclient\cefclient.o out\Release\obj\gcccefclient\cefclient\string_util.o out\Release\obj\gcccefclient\cefclient\client_app_delegates.o out\Release\obj\gcccefclient\cefclient\performance_test.o out\Release\obj\gcccefclient\cefclient\client_handler.o out\Release\obj\gcccefclient\cefclient\scheme_test.o out\Release\obj\gcccefclient\cefclient\resource_util_win.o out\Release\obj\gcccefclient\cefclient\client_handler_win.o out\Release\obj\gcccefclient\cefclient\binding_test.o  $(Release_Library_Path) $(Release_Libraries) -Wl,-rpath,.\ -o out\gccRelease\cefclient.exe

# Compiles file cefclient\performance_test_tests.cpp for the Release configuration...
-include out\Release\obj\gcccefclient\cefclient\performance_test_tests.d
out\Release\obj\gcccefclient\cefclient\performance_test_tests.o: cefclient\performance_test_tests.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c cefclient\performance_test_tests.cpp $(Release_Include_Path) -o out\Release\obj\gcccefclient\cefclient\performance_test_tests.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM cefclient\performance_test_tests.cpp $(Release_Include_Path) > out\Release\obj\gcccefclient\cefclient\performance_test_tests.d

# Compiles file cefclient\cefclient_win.cpp for the Release configuration...
-include out\Release\obj\gcccefclient\cefclient\cefclient_win.d
out\Release\obj\gcccefclient\cefclient\cefclient_win.o: cefclient\cefclient_win.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c cefclient\cefclient_win.cpp $(Release_Include_Path) -o out\Release\obj\gcccefclient\cefclient\cefclient_win.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM cefclient\cefclient_win.cpp $(Release_Include_Path) > out\Release\obj\gcccefclient\cefclient\cefclient_win.d

# Compiles file cefclient\window_test_win.cpp for the Release configuration...
-include out\Release\obj\gcccefclient\cefclient\window_test_win.d
out\Release\obj\gcccefclient\cefclient\window_test_win.o: cefclient\window_test_win.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c cefclient\window_test_win.cpp $(Release_Include_Path) -o out\Release\obj\gcccefclient\cefclient\window_test_win.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM cefclient\window_test_win.cpp $(Release_Include_Path) > out\Release\obj\gcccefclient\cefclient\window_test_win.d

# Compiles file cefclient\client_switches.cpp for the Release configuration...
-include out\Release\obj\gcccefclient\cefclient\client_switches.d
out\Release\obj\gcccefclient\cefclient\client_switches.o: cefclient\client_switches.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c cefclient\client_switches.cpp $(Release_Include_Path) -o out\Release\obj\gcccefclient\cefclient\client_switches.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM cefclient\client_switches.cpp $(Release_Include_Path) > out\Release\obj\gcccefclient\cefclient\client_switches.d

# Compiles file cefclient\osrenderer.cpp for the Release configuration...
-include out\Release\obj\gcccefclient\cefclient\osrenderer.d
out\Release\obj\gcccefclient\cefclient\osrenderer.o: cefclient\osrenderer.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c cefclient\osrenderer.cpp $(Release_Include_Path) -o out\Release\obj\gcccefclient\cefclient\osrenderer.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM cefclient\osrenderer.cpp $(Release_Include_Path) > out\Release\obj\gcccefclient\cefclient\osrenderer.d

# Compiles file cefclient\dom_test.cpp for the Release configuration...
-include out\Release\obj\gcccefclient\cefclient\dom_test.d
out\Release\obj\gcccefclient\cefclient\dom_test.o: cefclient\dom_test.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c cefclient\dom_test.cpp $(Release_Include_Path) -o out\Release\obj\gcccefclient\cefclient\dom_test.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM cefclient\dom_test.cpp $(Release_Include_Path) > out\Release\obj\gcccefclient\cefclient\dom_test.d

# Compiles file cefclient\dialog_test.cpp for the Release configuration...
-include out\Release\obj\gcccefclient\cefclient\dialog_test.d
out\Release\obj\gcccefclient\cefclient\dialog_test.o: cefclient\dialog_test.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c cefclient\dialog_test.cpp $(Release_Include_Path) -o out\Release\obj\gcccefclient\cefclient\dialog_test.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM cefclient\dialog_test.cpp $(Release_Include_Path) > out\Release\obj\gcccefclient\cefclient\dialog_test.d

# Compiles file cefclient\client_renderer.cpp for the Release configuration...
-include out\Release\obj\gcccefclient\cefclient\client_renderer.d
out\Release\obj\gcccefclient\cefclient\client_renderer.o: cefclient\client_renderer.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c cefclient\client_renderer.cpp $(Release_Include_Path) -o out\Release\obj\gcccefclient\cefclient\client_renderer.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM cefclient\client_renderer.cpp $(Release_Include_Path) > out\Release\obj\gcccefclient\cefclient\client_renderer.d

# Compiles file cefclient\client_app.cpp for the Release configuration...
-include out\Release\obj\gcccefclient\cefclient\client_app.d
out\Release\obj\gcccefclient\cefclient\client_app.o: cefclient\client_app.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c cefclient\client_app.cpp $(Release_Include_Path) -o out\Release\obj\gcccefclient\cefclient\client_app.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM cefclient\client_app.cpp $(Release_Include_Path) > out\Release\obj\gcccefclient\cefclient\client_app.d

# Compiles file cefclient\window_test.cpp for the Release configuration...
-include out\Release\obj\gcccefclient\cefclient\window_test.d
out\Release\obj\gcccefclient\cefclient\window_test.o: cefclient\window_test.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c cefclient\window_test.cpp $(Release_Include_Path) -o out\Release\obj\gcccefclient\cefclient\window_test.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM cefclient\window_test.cpp $(Release_Include_Path) > out\Release\obj\gcccefclient\cefclient\window_test.d

# Compiles file cefclient\cefclient_osr_widget_win.cpp for the Release configuration...
-include out\Release\obj\gcccefclient\cefclient\cefclient_osr_widget_win.d
out\Release\obj\gcccefclient\cefclient\cefclient_osr_widget_win.o: cefclient\cefclient_osr_widget_win.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c cefclient\cefclient_osr_widget_win.cpp $(Release_Include_Path) -o out\Release\obj\gcccefclient\cefclient\cefclient_osr_widget_win.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM cefclient\cefclient_osr_widget_win.cpp $(Release_Include_Path) > out\Release\obj\gcccefclient\cefclient\cefclient_osr_widget_win.d

# Compiles file cefclient\cefclient.cpp for the Release configuration...
-include out\Release\obj\gcccefclient\cefclient\cefclient.d
out\Release\obj\gcccefclient\cefclient\cefclient.o: cefclient\cefclient.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c cefclient\cefclient.cpp $(Release_Include_Path) -o out\Release\obj\gcccefclient\cefclient\cefclient.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM cefclient\cefclient.cpp $(Release_Include_Path) > out\Release\obj\gcccefclient\cefclient\cefclient.d

# Compiles file cefclient\string_util.cpp for the Release configuration...
-include out\Release\obj\gcccefclient\cefclient\string_util.d
out\Release\obj\gcccefclient\cefclient\string_util.o: cefclient\string_util.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c cefclient\string_util.cpp $(Release_Include_Path) -o out\Release\obj\gcccefclient\cefclient\string_util.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM cefclient\string_util.cpp $(Release_Include_Path) > out\Release\obj\gcccefclient\cefclient\string_util.d

# Compiles file cefclient\client_app_delegates.cpp for the Release configuration...
-include out\Release\obj\gcccefclient\cefclient\client_app_delegates.d
out\Release\obj\gcccefclient\cefclient\client_app_delegates.o: cefclient\client_app_delegates.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c cefclient\client_app_delegates.cpp $(Release_Include_Path) -o out\Release\obj\gcccefclient\cefclient\client_app_delegates.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM cefclient\client_app_delegates.cpp $(Release_Include_Path) > out\Release\obj\gcccefclient\cefclient\client_app_delegates.d

# Compiles file cefclient\performance_test.cpp for the Release configuration...
-include out\Release\obj\gcccefclient\cefclient\performance_test.d
out\Release\obj\gcccefclient\cefclient\performance_test.o: cefclient\performance_test.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c cefclient\performance_test.cpp $(Release_Include_Path) -o out\Release\obj\gcccefclient\cefclient\performance_test.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM cefclient\performance_test.cpp $(Release_Include_Path) > out\Release\obj\gcccefclient\cefclient\performance_test.d

# Compiles file cefclient\client_handler.cpp for the Release configuration...
-include out\Release\obj\gcccefclient\cefclient\client_handler.d
out\Release\obj\gcccefclient\cefclient\client_handler.o: cefclient\client_handler.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c cefclient\client_handler.cpp $(Release_Include_Path) -o out\Release\obj\gcccefclient\cefclient\client_handler.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM cefclient\client_handler.cpp $(Release_Include_Path) > out\Release\obj\gcccefclient\cefclient\client_handler.d

# Compiles file cefclient\scheme_test.cpp for the Release configuration...
-include out\Release\obj\gcccefclient\cefclient\scheme_test.d
out\Release\obj\gcccefclient\cefclient\scheme_test.o: cefclient\scheme_test.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c cefclient\scheme_test.cpp $(Release_Include_Path) -o out\Release\obj\gcccefclient\cefclient\scheme_test.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM cefclient\scheme_test.cpp $(Release_Include_Path) > out\Release\obj\gcccefclient\cefclient\scheme_test.d

# Compiles file cefclient\resource_util_win.cpp for the Release configuration...
-include out\Release\obj\gcccefclient\cefclient\resource_util_win.d
out\Release\obj\gcccefclient\cefclient\resource_util_win.o: cefclient\resource_util_win.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c cefclient\resource_util_win.cpp $(Release_Include_Path) -o out\Release\obj\gcccefclient\cefclient\resource_util_win.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM cefclient\resource_util_win.cpp $(Release_Include_Path) > out\Release\obj\gcccefclient\cefclient\resource_util_win.d

# Compiles file cefclient\client_handler_win.cpp for the Release configuration...
-include out\Release\obj\gcccefclient\cefclient\client_handler_win.d
out\Release\obj\gcccefclient\cefclient\client_handler_win.o: cefclient\client_handler_win.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c cefclient\client_handler_win.cpp $(Release_Include_Path) -o out\Release\obj\gcccefclient\cefclient\client_handler_win.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM cefclient\client_handler_win.cpp $(Release_Include_Path) > out\Release\obj\gcccefclient\cefclient\client_handler_win.d

# Compiles file cefclient\binding_test.cpp for the Release configuration...
-include out\Release\obj\gcccefclient\cefclient\binding_test.d
out\Release\obj\gcccefclient\cefclient\binding_test.o: cefclient\binding_test.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c cefclient\binding_test.cpp $(Release_Include_Path) -o out\Release\obj\gcccefclient\cefclient\binding_test.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM cefclient\binding_test.cpp $(Release_Include_Path) > out\Release\obj\gcccefclient\cefclient\binding_test.d

# Creates the intermediate and output folders for each configuration...
.PHONY: create_folders
create_folders:
	mkdir -p out\Debug\obj\gcccefclient\source
	mkdir -p out\gccDebug
	mkdir -p out\Debug\obj\gcccefclient\source
	mkdir -p out\gccDebug
	mkdir -p out\Release\obj\gcccefclient\source
	mkdir -p out\gccRelease
	mkdir -p out\Release\obj\gcccefclient\source
	mkdir -p out\gccRelease

# Cleans intermediate and output files (objects, libraries, executables)...
.PHONY: clean
clean:
	rm -f out\Debug\obj\gcccefclient\*.o
	rm -f out\Debug\obj\gcccefclient\*.d
	rm -f out\gccDebug\*.a
	rm -f out\gccDebug\*.so
	rm -f out\gccDebug\*.dll
	rm -f out\gccDebug\*.exe
	rm -f out\Debug\obj\gcccefclient\*.o
	rm -f out\Debug\obj\gcccefclient\*.d
	rm -f out\gccDebug\*.a
	rm -f out\gccDebug\*.so
	rm -f out\gccDebug\*.dll
	rm -f out\gccDebug\*.exe
	rm -f out\Release\obj\gcccefclient\*.o
	rm -f out\Release\obj\gcccefclient\*.d
	rm -f out\gccRelease\*.a
	rm -f out\gccRelease\*.so
	rm -f out\gccRelease\*.dll
	rm -f out\gccRelease\*.exe
	rm -f out\Release\obj\gcccefclient\*.o
	rm -f out\Release\obj\gcccefclient\*.d
	rm -f out\gccRelease\*.a
	rm -f out\gccRelease\*.so
	rm -f out\gccRelease\*.dll
	rm -f out\gccRelease\*.exe

